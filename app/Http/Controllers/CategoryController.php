<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Classes\GetCollections;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
  function showCategory($id) {
    dd($id);

    $category = GetCollections::getCategoryCollection($id);


    return view('category', [
      'category' => GetCollections::getCategoryById($id),
      'categories' => GetCollections::getCategoriesCollection(),
      'products' => $category->products
    ]);
  }

  public function sort(Request $request) {
    $category = GetCollections::getCategoryCollection($request->category_id);

    if ($request->sort) {
      switch ($request->sort) {
        case 'DESC':
          $sortedProducts = $category->products->sortByDesc($request->q)->values()->all();
          break;
        case 'ASC':
          $sortedProducts = $category->products->sortBy($request->q)->values()->all();
          break;
      }
    }

    return view('category', [
      'category' => $category,
      'categories' => GetCollections::getCategoriesCollection(),
      'products' => $sortedProducts
    ]);
  }
}
