<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Classes\GetCollections;
use Illuminate\Http\Request;

class ProductController extends Controller
{
  /*function showProduct($id) {

    $category = GetCollections::getCategoryCollection($id);

    return view('category', [
      'category' => GetCollections::getCategoryById($id),
      'categories' => GetCollections::getCategoriesCollection(),
      'products' => $category->products
    ]);
  }*/
  function show() {
    $products = Product::paginate(10);

    return view("admin.pages.products", ["products" => $products]);
  }

  function create() {
    return view("admin.pages.product-form", ['categories' => GetCollections::getCategoriesCollection()]);
  }

  function store(Request $request) {
    $request->validate([
      'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      'title' => 'required',
      'price' => 'required',
      'year' => 'required',
      'country' => 'required',
      'director' => 'required',
      'play' => 'required',
      'cast' => 'required',
      'description' => 'required',
    ]);

    $input['category_id'] = $request->section_id;

    $imageName = strtok($request->image->getClientOriginalName(), '.');
    $input['image'] = $imageName . '.'.$request->image->getClientOriginalExtension();

    $input['title'] = $request->title;
    $input['price'] = $request->price;
    $input['year'] = $request->year;
    $input['country'] = $request->country;
    $input['director'] = $request->director;
    $input['play'] = $request->play;
    $input['cast'] = $request->cast;
    $input['description'] = $request->description;
    $input['date'] = strtotime(now());

    Product::create($input);

    return redirect()->route('admin.products')
      ->with('success', 'Product created successfully.');
  }

  public function edit($id)
  {
    $product = Product::find($id);

    return view('admin.pages.product-form', [
      'product' => $product,
      'categories' => GetCollections::getCategoriesCollection()
    ]);
  }

  public function update(Request $request)
  {
    $request->validate([
      'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      'title' => '',
      'price' => '',
      'year' => '',
      'country' => '',
      'director' => '',
      'play' => '',
      'cast' => '',
      'description' => '',
      'date' => ''
    ]);

    if (isset($request->image)) {
      $imageName = strtok($request->image->getClientOriginalName(), '.');
      $request->image = $imageName . '.' . $request->image->getClientOriginalExtension();
    }

    $request->date = strtotime(now());

    $product = Product::find($request->id);

    if (isset($product)) {
      dd($request->all());
      $product->update($request->all());
    }

    return redirect()->route('admin.products')
      ->with('success', 'Category updated successfully');
  }

  public function destroy($id) {
    $product = Product::find($id);
    $product->delete();

    return redirect()->route('admin.products')
      ->with('success', 'Contact deleted!');
  }
}
