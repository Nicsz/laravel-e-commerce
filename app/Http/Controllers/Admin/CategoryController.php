<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\Category;
use App\Classes\GetCollections;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
  function show() {

    $categories = GetCollections::getCategoriesCollection();

    return view("admin.pages.categories", ["categories" => $categories]);
  }

  function create() {
    return view("admin.pages.category-form");
  }

  function store(Request $request) {

    $request->validate([
      'title' => 'required',
    ]);

    /*$category = new Category([
      'title' => $request->get('title')
    ]);
    $category->save();*/

    Category::create($request->all());

    return redirect()->route('admin.categories')
      ->with('success', 'Product created successfully.');
  }

  public function edit($id)
  {
    $category = Category::find($id);

    return view('admin.pages.category-form', ['category' => $category]);
  }

  public function update(Request $request)
  {
    $request->validate([
      'title' => 'required',
    ]);

    $category = GetCollections::getCategoryById($request->id);

    $category->update($request->all());

    return redirect()->route('admin.categories')
      ->with('success', 'Category updated successfully');
  }

  public function destroy($id) {
    $contact = Category::find($id);
    $contact->delete();

    return redirect()->route('admin.categories')
      ->with('success', 'Contact deleted!');
  }
}
