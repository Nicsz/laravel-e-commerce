<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
  //
  public function index() {
    $login = session()->get('login');

    if (isset($login)) {
      return view('admin.index');
    }

    return redirect()->route('admin.auth');
  }

  public function auth() {
    $login = session()->get('login');

    if (isset($login)) {
      return redirect()->route('admin.main');
    }

    return view('admin.auth');
  }

  public function login(Request $request) {
    $secret = bcrypt(env('ADMIN_SECRET'));
    $password = Hash::make(env('ADMIN_PASSWORD') . $secret);

    $request->validate([
      'login' => 'required',
      'password' => 'required',
    ]);

    if (($request->login == env('ADMIN_LOGIN')) && Hash::check($request->password . $secret, $password)) {

      $login = session()->get('login');

      if(!$login) {

        $login = [
          "login"    => $request->login,
          "password" => $password,
        ];

        session()->put('login', $login);
      }

      return redirect()->route('admin.main')
      ->with('success', 'Login Successfully');
    }
  }

  public function adminExit() {

    $login = session()->get('login');
    unset($login);

    return redirect()->route('admin.auth');
  }
}
