<?php

namespace App\Http\Controllers;

use App\Classes\GetCollections;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
  public function index() {
    return view('pages.order', [
      'categories' => GetCollections::getCategoriesCollection()
    ]);
  }
}
