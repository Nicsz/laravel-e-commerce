<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Classes\GetCollections;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
  function index() {
    GetCollections::getProductsCollection();
  }

  function show($id) {

    $product = Product::find($id);

    $category_id = Product::select('category_id')->where('id', $id)->get();

    $category = Category::find($category_id[0]['category_id']);

//    $others = "SELECT * FROM `".$this->table_name."` WHERE `section_id`=".$this->config->sym_query." AND `id` != ".$this->config->sym_query." ORDER BY RAND() $l";

    $other_products = DB::table('sdvd_products')->where('category_id', $category_id[0]['category_id'])->where('id', '!=', $id)->inRandomOrder()->get();

//    dd($others);

    return view('pages.product', [
      'product'  => $product,
      'products' => $other_products,
      'category'  => $category,
      'categories' => Category::all(),
    ]);
  }

  public function searchProduct(Request $request) {

    // Sets the parameters from the get request to the variables.
    $title = $request->input('q');

    $products = Product::where('title', 'LIKE', '%' . $title . '%')->orderBy('title', 'asc')->get();

//    dd(count($products) === 0);

    return view('pages.search', [
      'products' => $products,
      'categories' => Category::all(),
    ]);
  }

  public function priceSort(Request $request) {

    $products = Product::orderBy('price', $request->sort)->get();

    return view('index', [
      'products' => $products,
      'categories' => Category::all(),
    ]);
  }
}
