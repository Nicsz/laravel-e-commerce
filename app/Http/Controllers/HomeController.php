<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;
use Illuminate\View\View;

class HomeController extends Controller
{
  /*protected $title = "Интернет-магазин";
  protected $meta_desc = "Интернет-магазин по продаже DVD-дисков.";
  protected $meta_key = "интернет магазин, интернет магазин dvd, интернет магазин dvd диски";*/

  function index(Request $request) {

    $products = Product::orderBy('date','desc')->take(8)->get();

    if ($request->sort) {
      switch ($request->sort) {
        case 'DESC':
          $products = $products->sortByDesc($request->q)->values()->all();
          break;
        case 'ASC':
          $products = $products->sortBy($request->q)->values()->all();
          break;
      }
    }

    return view('index', [
        'products' => $products,
        'categories' => Category::all(),
      ]
    );
  }
}
