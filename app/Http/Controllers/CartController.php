<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Classes\GetCollections;
use Illuminate\Http\Request;

class CartController extends Controller
{
  public function cart()
  {
    return view('pages.cart', [
      'categories' => GetCollections::getCategoriesCollection()
    ]);
  }

  public function addToCart($id)
  {
    $product = Product::find($id);

    if(!$product) {
      abort(404);
    }

    $cart = session()->get('cart');

    // if cart is empty then this the first product
    if(!$cart) {

      $cart = [
        $id => [
          "id" => $product->id,
          "img" => $product->img,
          "title" => $product->title,
          "quantity" => 1,
          "price" => $product->price
        ]
      ];

      session()->put('cart', $cart);

      return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

    // if cart not empty then check if this product exist then increment quantity
    if(isset($cart[$id])) {

      $cart[$id]['quantity']++;

      session()->put('cart', $cart);

      return redirect()->back()->with('success', 'Product added to cart successfully!');

    }

    // if item not exist in cart then add to cart with quantity = 1
    $cart[$id] = [
      "id" => $product->id,
      "img" => $product->img,
      "title" => $product->title,
      "quantity" => 1,
      "price" => $product->price
    ];

    session()->put('cart', $cart);

    return redirect()->back()->with('success', 'Product added to cart successfully!');
  }

  public function update(Request $request)
  {
    if ($request->quantity <= 0)
    {
      $cart = session()->get('cart');

      $cart[$request->id]["quantity"] = 1;

      session()->put('cart', $cart);

      return view('pages.cart');
    }

    if ($request->id and $request->quantity)
    {
      $cart = session()->get('cart');

      $cart[$request->id]["quantity"] = $request->quantity;

      session()->put('cart', $cart);

      session()->flash('success', 'Cart updated successfully');
    }
  }

  public function remove(Request $request)
  {
    if($request->id) {

      $cart = session()->get('cart');

      if(isset($cart[$request->id])) {

        unset($cart[$request->id]);

        session()->put('cart', $cart);
      }

      session()->flash('success', 'Product removed successfully');
    }
  }
}