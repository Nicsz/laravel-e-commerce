<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
  protected $table = "sdvd_products";

  public $timestamps = false;

  protected $fillable = ['category_id', 'title', 'price', 'year', 'country', 'director', 'play', 'cast', 'description', 'image', 'date'];
}
