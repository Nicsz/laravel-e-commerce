<?php


namespace App\Classes;

use App\Category;
use App\Product;

class GetCollections
{
  public static function getProductsCollection() {
    $products = Product::all();

    return $products;
  }

  public static function getProductById($id) {
    $product = Category::find($id);

    return $product;
  }

  public static function getCategoriesCollection() {
    $categories = Category::all();

    return $categories;
  }

  public static function getCategoryById($id) {
    $category = Category::find($id);

    return $category;
  }

  public static function getCategoryCollection($id) {
    $category = Category::with('products')->findOrFail($id);

    return $category;
  }
}