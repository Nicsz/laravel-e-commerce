<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
  protected $table = "sdvd_categories";

  protected $fillable = ['title'];

  public function products()
  {
    return $this->hasMany(Product::class);
  }
}
