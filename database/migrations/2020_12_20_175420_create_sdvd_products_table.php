<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSdvdProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdvd_products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('section_id')->unsigned();
            $table->string('img', 255)->nullable();
            $table->string('title', 255)->nullable();
            $table->double('price')->unsigned();
            $table->string('year')->nullable();
            $table->string('country', 255)->nullable();
            $table->string('director', 255)->nullable();
            $table->string('play', 255)->nullable();
            $table->text('cast')->nullable();
            $table->text('description')->nullable();
            $table->string('date', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sdvd_products');
    }
}
