<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?/*=$this->title*/?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="<?/*=$this->meta_desc*/?>" />
  <meta name="keywords" content="<?/*=$this->meta_key*/?>" />
  <link rel="stylesheet" href="css/main.css" type="text/css" />
  <!--[if IE 8]>
  <link rel="stylesheet" href="css/ie8.css" type="text/css" />
  <![endif]-->
  <script type="text/javascript" src="js/functions.js"></script>
  <link href="favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
<div id="container">
  <div id="header">
    <img src="images/header.png" alt="Шапка" />
    <div>
      <p class="red">8-800-123-45-67</p>
      <p class="blue">Время работы с 09:00 до 21:00<br />без перерыва и выходных</p>
    </div>
    <div class="cart">
      <p class="cart_title">Корзина</p>
      <p class="blue">Текущий заказ</p>
      <p>В корзине <span><?/*=$this->cart_count*/?></span> <?/*=$this->cart_word*/?><br />на сумму <span><?/*=$this->cart_summa*/?></span> грн.</p>
      <a href="<?/*=$this->cart_link*/?>">Перейти в корзину</a>
    </div>
  </div>
  <div id="topmenu">
    <ul>
      <li>
        <a href="<?/*=$this->index*/?>">ГЛАВНАЯ</a>
      </li>
      <li>
        <img src="images/topmenu_border.png" alt="" />
      </li>
      <li>
        <a href="<?/*=$this->link_delivery*/?>">ОПЛАТА И ДОСТАВКА</a>
      </li>
      <li>
        <img src="images/topmenu_border.png" alt="" />
      </li>
      <li>
        <a href="{{ route('contacts') }}">КОНТАКТЫ</a>
      </li>
    </ul>
    <div id="search">
      <form name="search" action="<?/*=$this->link_search*/?>" method="get">
        <table>
          <tr>
            <td class="input_left"></td>
            <td>
              <input type="text" name="q" value="поиск" onfocus="if(this.value == 'поиск') this.value=''" onblur="if(this.value == '') this.value='поиск'" />
            </td>
            <td class="input_right"></td>
          </tr>
        </table>
      </form>
    </div>
  </div>
  <div id="content">
    @yield('content')
  </div>
</div>
</body>
</html>