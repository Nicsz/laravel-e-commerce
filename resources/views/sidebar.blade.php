<div id="left">
  <div id="menu">
    <div class="header">
      <h3>Разделы</h3>
    </div>

    <div id="items">

      @foreach($categories as $id => $category)
        <p @if ($id + 1 == count($categories)) class="last" @endif>
          <a href="/category/{{ $category->id }}">{{ $category->title }}</a>
        </p>
      @endforeach
    </div>
    <div class="bottom"></div>
  </div>
</div>