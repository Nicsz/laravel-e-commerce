@section('title', $category->title)

@extends('default')

@section('content')

<table>
  <tr>
    <td rowspan="2">
      <div class="header">
        <h3> @yield('title') </h3>
      </div>
    </td>
    <td class="category_bg"></td>
    <td class="category_right"></td>
  </tr>
  <tr>
    <td colspan="2">
      <table class="sort">
        <tr>
          <td>Сортировать по:</td>
{{--          <td>цене (<a href="<?/*=$this->link_price_up*/?>">возр.</a> | <a href="<?/*=$this->link_price_down*/?>">убыв.</a>)--}}
          <td>цене (<a href="{{ url('/category/'.$category->id.'/sort') }}?category_id={{ $category->id }}&q=price&sort=ASC">возр.</a> | <a href="{{ url('/category/'.$category->id.'/sort') }}?category_id={{ $category->id }}&q=price&sort=DESC">убыв.</a>)
          <td>названию (<a href="{{ url('/category/'.$category->id.'/sort') }}?category_id={{ $category->id }}&q=title&sort=ASC">возр.</a> | <a href="{{ url('/category/'.$category->id.'/sort') }}?category_id={{ $category->id }}&q=title&sort=DESC">убыв.</a>)
        </tr>
      </table>
    </td>
  </tr>
</table>
<table class="products">

  @foreach($products as $id => $product)
    @if ($id % 4 == 0)
      <tr>
        @endif
        <td>
          <div class="intro_product">
            <p class="img">
              <img src="{{asset("/images/products/$product->img")}}" alt="{{ $product->title }}" />
            </p>
            <p class="title">
              <a href="/product/{{ $product->id }}">{{ $product->title }}</a>
            </p>
            <p class="price">{{ $product->price }} грн.</p>
            <p>
              <a class="link_cart" href="{{ $product->link_cart }}"></a>
            </p>
          </div>
        </td>
        @if (($id + 1) % 4 == 0)
      </tr>
    @endif
  @endforeach
</table>

@endsection