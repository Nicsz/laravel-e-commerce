<div id="header">
  <img src="{{ asset('images/header.png') }}" alt="Шапка" />
  <div>
    <p class="red">8-800-123-45-67</p>
    <p class="blue">Время работы с 09:00 до 21:00<br />без перерыва и выходных</p>
  </div>
  <div class="cart">
    <p class="cart_title">Корзина</p>
    <p class="blue">Текущий заказ</p>

    <?php $total = 0 ?>
    @foreach((array) session('cart') as $id => $details)
      <?php $total += $details['price'] * $details['quantity'] ?>
    @endforeach

    <p>В корзине <span>{{ count((array) session('cart')) }}</span> <?/*=$this->cart_word*/?><br />на сумму <span>{{ $total }}</span> грн.</p>
    <a href="{{ url('cart') }}">
      Перейти в корзину
{{--      <i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>--}}
    </a>
  </div>
</div>
<div id="topmenu">
  <ul>
    <li>
      <a href="{{ route('home') }}">ГЛАВНАЯ</a>
    </li>
    <li>
      <img src="{{ asset('images/topmenu_border.png') }}" alt="" />
    </li>
    <li>
      <a href="{{ route('delivery') }}">ОПЛАТА И ДОСТАВКА</a>
    </li>
    <li>
      <img src="{{ asset('images/topmenu_border.png') }}" alt="" />
    </li>
    <li>
      <a href="{{ route('contacts') }}">КОНТАКТЫ</a>
    </li>
  </ul>
  <div id="search">
{{--    <form name="search" action="@if(str_contains(url()->current(), 'section')) {{ route('search-product') }} @else {{ route('search-section-product') }} @endif" method="get">--}}
    <form name="search" action="{{ route('search-products') }}" method="get">
      <table>
        <tr>
          <td class="input_left"></td>
          <td>
            <input type="text" name="q" value="поиск" onfocus="if(this.value == 'поиск') this.value=''" onblur="if(this.value == '') this.value='поиск'" />
          </td>
          <td class="input_right"></td>
        </tr>
      </table>
    </form>
  </div>
</div>