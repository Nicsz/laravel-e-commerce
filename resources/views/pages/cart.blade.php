@extends('default')

@section('title', 'Cart')

@section('content')

  <div id="cart">
    <h2>Корзина</h2>
      <table>
        <tr>
          <td colspan="8" id="cart_top"></td>
        </tr>
        <tr>
          <td class="cart_left"></td>
          <td colspan="2">Товар</td>
          <td>Цена за 1 шт.</td>
          <td>Количество</td>
          <td>Стоимость</td>
          <td></td>
          <td class="cart_right"></td>
        </tr>
        <tr>
          <td class="cart_left"></td>
          <td colspan="6">
            <hr />
          </td>
          <td class="cart_right"></td>
        </tr>

        <?php $total = 0 ?>

        @if(session('cart'))
          @foreach(session('cart') as $id => $product)

        <?php $total += $product['price'] * $product['quantity'] ?>

<!--        --><?php //for ($i = 0; $i < count($this->cart); $i++) { ?>
        <tr class="cart_row">
          <td class="cart_left"></td>
          <td class="img">
            <img src="{{asset("/images/products/".$product['img'])}}" alt="{{ $product['title'] }}" />
          </td>
          <td class="title">{{ $product['title'] }}</td>
          <td>{{ $product['price'] }} грн.</td>
          <td>
            <table class="count">
              <tr>
                <td>
                  <input type="text" name="count_{{ $product['id'] }}" class="quantity" value="{{ $product['quantity'] }}" />
                </td>
                <td>шт.</td>
              </tr>
            </table>
          </td>
          <td class="bold">{{ $product['price'] * $product['quantity'] }} грн.</td>
          <td>
            <button class="btn btn-info btn-sm update-cart" data-id="{{ $id }}"><i class="fas fa-sync-alt"></i></button>
            <button class="btn btn-danger btn-sm remove-from-cart link_delete" data-id="{{ $id }}">x удалить</button>
{{--            <a href="<?/*=$this->cart[$i]["link_delete"]*/?>" class="link_delete"></a>--}}
          </td>
          <td class="cart_right"></td>
        </tr>
<!--        --><?php //if ($i + 1 != count($this->cart)) { ?>
        <tr>
          <td class="cart_left"></td>
          <td colspan="6" class="cart_border">
            <hr />
          </td>
          <td class="cart_right"></td>
        </tr>
<!--        --><?php //} ?>
<!--        --><?php //} ?>
          @endforeach
        @endif
        <tr id="discount">
          <td class="cart_left"></td>
          <td colspan="6">
            <form name="discount" action="#" method="post">
              <table>
                <tr>
                  <td>Введите номер купона со скидкой<br /><span>(если есть)</span></td>
                  <td>
                    <input type="text" name="discount" value="<?/*=$this->discount*/?>" />
                  </td>
                  <td>
                    <input type="image" src="/images/cart_discount.png" alt="Получить скидку" onmouseover="this.src='/images/cart_discount_active.png'" onmouseout="this.src='/images/cart_discount.png'" />
                  </td>
                </tr>
              </table>
            </form>
          </td>
          <td class="cart_right"></td>
        </tr>
        <tr id="summa">
          <td class="cart_left"></td>
          <td colspan="6">
            <p>Итого: {{ $total }} грн. <?php /*if ($this->discount) { */?><!--(с учётом скидки)<?php /*}*/?>: <span><?/*=$this->summa*/?> грн.</span>--></p>
          </td>
          <td class="cart_right"></td>
        </tr>
        <tr>
          <td class="cart_left"></td>
          <td colspan="2">
            <div class="left">
              <div></div>
            </div>
          </td>
          <td colspan="4">
            <div class="right">
              <input type="hidden" name="func" value="cart" />
              <a href="{{ route('order') }}">
                <img src="/images/cart_order.png" alt="Оформить заказ" onmouseover="this.src='/images/cart_order_active.png'" onmouseout="this.src='/images/cart_order.png'" />
              </a>
            </div>
          </td>
          <td class="cart_right"></td>
        </tr>
        <tr>
          <td colspan="8" id="cart_bottom"></td>
        </tr>
      </table>
  </div>

@endsection

@section('scripts')

  <script type="text/javascript">

    $(".update-cart").click(function (e) {
      e.preventDefault();

      let ele = $(this);

      $.ajax({
        url: '{{ url('update-cart') }}',
        method: "patch",
        data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id"), quantity: ele.parents("tr").find(".quantity").val()},
        success: function (response) {
          window.location.reload();
        }
      });
    });

    $(".remove-from-cart").click(function (e) {
      e.preventDefault();

      let ele = $(this);

      if(confirm("Are you sure")) {
        $.ajax({
          url: '{{ url('remove-from-cart') }}',
          method: "DELETE",
          data: {_token: '{{ csrf_token() }}', id: ele.attr("data-id")},
          success: function (response) {
            window.location.reload();
          }
        });
      }
    });

  </script>

@endsection