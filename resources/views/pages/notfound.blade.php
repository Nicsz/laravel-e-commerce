@extends('default')

@section('content')

  <div id="message">
    <h2>Страница не существует</h2>
    <p>Возможно, Вы ошиблись при вводе адреса.<br />Вернуться на <a href="{{ route('home') }}">главную</a>.</p>
  </div>

@endsection