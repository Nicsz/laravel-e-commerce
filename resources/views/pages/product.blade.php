@extends('default')

@section('content')

<!--  --><?php //dd($section);?>

  <table id="hornav">
    <tr>
      <td>
        <a href="{{ route('home') }}">Главная</a>
      </td>
      <td>
        <img src="{{ asset('/images/hornav_arrow.png') }}" alt="" />
      </td>
      <td>
        <a href="/category/{{ $category->id }}">{{ $category->title }}</a>
      </td>
      <td>
        <img src="{{ asset('/images/hornav_arrow.png') }}" alt="" />
      </td>
      <td>{{ $product->title }}</td>
    </tr>
  </table>
  <table id="product">
    <tr>
      <td class="product_img">
        <img src="{{ asset('/images/products/' . $product->image) }}" alt="{{ $product->title }}" />
      </td>
      <td class="product_desc">
        <p>Название: <span class="title">{{ $product->title }}</span></p>
        <p>Год выхода: <span>{{ $product->year }}</span></p>
        <p>Жанр: <span><?/*=$this->product["section"]*/?></span></p>
        <p>Страна-производитель: <span>{{ $product->country }}</span></p>
        <p>Режиссёр: <span>{{ $product->director }}</span></p>
        <p>Продолжительность: <span>{{ $product->play }}</span></p>
        <p>В ролях: <span>{{ $product->cast }}</span></p>
        <table>
          <tr>
            <td>
              <p class="price">{{ $product->price }} грн.</p>
            </td>
            <td>
              <p>
                <a class="link_cart" href="{{ url('add-to-cart/'.$product->id) }}"></a>
              </p>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <p class="desc_title">Описание:</p>
        <p class="desc">{{ $product->description }}</p>
      </td>
    </tr>
  </table>

  <div id="others">
    <h3>С этим товаром также заказывают:</h3>
    <table class="products">
      <tr>

        @foreach($products as $product)

          <td>
            <div class="intro_product">
              <p class="img">
                <img src="{{asset("/images/products/$product->img")}}" alt="{{ $product->title }}" />
              </p>
              <p class="title">
                <a href="/product/{{ $product->id }}">{{ $product->title }}</a>
              </p>
              <p class="price">{{ $product->price }} грн.</p>
              <p>
                <a class="link_cart" href="<?/*=$this->products[$i]["link_cart"]*/?>"></a>
              </p>
            </div>
          </td>

        @endforeach

      </tr>
    </table>
  </div>

@endsection