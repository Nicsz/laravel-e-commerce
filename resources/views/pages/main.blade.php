@section('title','Новинки')

<table>
  <tr>
    <td rowspan="2">
      <div class="header">
        <h3> @yield('title') </h3>
      </div>
    </td>
    <td class="section_bg"></td>
    <td class="section_right"></td>
  </tr>
  <tr>
    <td colspan="2">
      <table class="sort">
        <tr>
          <td>Сортировать по:</td>
          <td>цене (<a href="{{ url('/sort') }}?q=price&sort=ASC">возр.</a> | <a href="{{ url('/sort') }}?q=price&sort=DESC">убыв.</a>)
          <td>названию (<a href="{{ url('/sort') }}?q=title&sort=ASC">возр.</a> | <a href="{{ url('/sort') }}?q=title&sort=DESC">убыв.</a>)
        </tr>
      </table>
    </td>
  </tr>
</table>
<table class="products">

  @foreach($products as $id => $product)
    @if ($id % 4 == 0)
      <tr>
        @endif
        <td>
          <div class="intro_product">
            <p class="img">
              <img src="{{asset("/images/products/$product->image")}}" alt="{{ $product->title }}" />
            </p>
            <p class="title">
              <a href="/product/{{ $product->id }}">{{ $product->title }}</a>
            </p>
            <p class="price">{{ $product->price }} грн.</p>
            <p>
              <a class="link_cart" href="{{ url('add-to-cart/'.$product->id) }}"></a>
            </p>
          </div>
        </td>
        @if (($id + 1) % 4 == 0)
      </tr>
    @endif
  @endforeach

</table>