@section('title', 'Поиск')

@extends('default')

@section('content')

  <div id="search_result">

<!--    --><?php //dd($products); ?>

    @if(empty($_GET['q']))

      <h2>Вы задали пустой поисковый запрос!</h2>

    @elseif(count($products) === 0)

      <h2>Результаты поиска: {{ $_GET['q'] }}</h2>
      <p>Ничего не найдено</p>

    @else

      <h2>Результаты поиска: {{ $_GET['q'] }}</h2>

      @include('pages.main')

    @endif

  </div>
@endsection