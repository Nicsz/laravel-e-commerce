<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?/*=$this->title*/?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="<?/*=$this->meta_desc*/?>" />
  <meta name="keywords" content="<?/*=$this->meta_key*/?>" />
  <link type="text/css" rel="stylesheet" href="{{ asset('/admin/css/main.css') }}" />
  <script type="text/javascript" src="{{ asset('/admin/js/functions.js') }}"></script>
</head>
<body>
<div id="container">
  <div id="header">
    <h1>Аккаунт администратора</h1>
  </div>
  <hr />

  @include('admin.menu')

  <div id="content">

    @yield('content')

  </div>
</div>
</body>
</html>