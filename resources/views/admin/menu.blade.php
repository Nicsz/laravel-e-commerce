{{--@auth--}}
  <div id="menu">
    <h2>Меню</h2>
    <table>
      <tr>
        <td>
          <a href="{{ route('admin.main') }}">Главная</a>
        </td>
        <td>
          <a href="{{ route('admin.products') }}">Товары</a>
        </td>
        <td>
          <a href="<?/*=$this->link_orders*/?>">Заказы</a>
        </td>
      </tr>
      <tr>
        <td>
          <a href="{{ route('admin.categories') }}">Категории</a>
        </td>
        <td>
          <a href="<?/*=$this->link_discounts*/?>">Купоны</a>
        </td>
        <td>
          <a href="/admin/statistics">Статистика</a>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <a href="/admin/exit">Выход</a>
        </td>
      </tr>
    </table>
  </div>
  <hr />
{{--@endauth--}}