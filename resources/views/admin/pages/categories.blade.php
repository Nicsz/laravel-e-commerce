@extends('admin.default')

@section('content')

  <h3>Разделы</h3>
  <?php //include "message.tpl"; ?>
  <?php //include "pagination.tpl"; ?>
  <p class="link_new">
    <a href="/admin/category/new">Добавить новый раздел</a>
  </p>
  <table class="info">
    <tr class="header">
      <td>ID</td>
      <td>Название</td>
      <td>Функции</td>
    </tr>

    @foreach($categories as $key => $category)
      <tr>
        <td>{{ ++$key }}</td>
        <td>{{ $category->title }}</td>
        <td>
          <a href="/admin/category/{{ $category->id }}/edit">Редактировать</a>
          <br />
{{--          <a href="/admin/category/{{ $category->id }}/delete" onclick="return confirm('Вы уверены, что хотите удалить элемент?')">Удалить</a>--}}
          <form action="{{ route('admin.category.destroy', $category->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger" type="submit">Удалить</button>
          </form>
        </td>
      </tr>

    @endforeach

  </table>

@endsection