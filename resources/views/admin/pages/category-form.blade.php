@extends('admin.default')

@section('content')
{{--  <h3><?=$this->form_title?></h3>--}}
<!--  --><?php //include "message.tpl"; ?>
  <div class="form">

    @if(isset($category))

      <form name="section" action="{{ route('admin.category.update') }}" method="POST">

        @method('PUT')

    @else

      <form name="section" action="{{ route('admin.category.save') }}" method="POST">

    @endif

      @csrf

      <table>
        <tr>
          <td>Название:</td>
          <td>
            <input type="text" name="title" value="{{ isset($category) ? $category->title : '' }}" />
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <input type="hidden" name="id" value="{{ isset($category) ? $category->id : '' }}" />
{{--            <input type="hidden" name="func" value="<?/*=$this->func*/?>" />--}}
            <input type="submit" value="Отправить" />
          </td>
        </tr>
      </table>
    </form>
  </div>

@endsection