@extends('admin.default')

@section('content')

  <h3>Товары</h3>
  <?php //include "message.tpl"; ?>
  <?php //include "pagination.tpl"; ?>
  <p class="link_new">
    <a href="/admin/product/new">Добавить новый товар</a>
  </p>
  <table class="info">
    <tr class="header">
      <td>ID</td>
      <td>Раздел</td>
      <td>Название</td>
      <td>Цена</td>
      <td>Информация</td>
      <td>Дата добавления</td>
      <td>Функции</td>
    </tr>

    @foreach($products as $product)

      <tr>
        <td>{{ $product->id }}</td>
        <td><?/*=$data["section"]*/?></td>
        <td>
          {{ $product->title }}
          <br />
          <img class="img" src="{{ asset('/images/products/' . $product->image) }}" alt="{{ $product->title }}" />
        </td>
        <td>{{ $product->price }} гривен</td>
        <td>
          <table>
            <tr>
              <td><b>Год:</b></td>
              <td>{{ $product->year }}</td>
            </tr>
            <tr>
              <td><b>Страна:</b></td>
              <td>{{ $product->country }}</td>
            </tr>
            <tr>
              <td><b>Режиссёр:</b></td>
              <td>{{ $product->director }}</td>
            </tr>
            <tr>
              <td><b>Продолжительность:</b></td>
              <td>{{ $product->play }}</td>
            </tr>
          </table>
        </td>
        <td>{{ date('Y-m-d H:i:s', $product->date ) }}</td>
        <td>
          <a href="/admin/product/{{ $product->id }}/edit">Редактировать</a>
          <br />
          {{--          <a href="/admin/category/{{ $category->id }}/delete" onclick="return confirm('Вы уверены, что хотите удалить элемент?')">Удалить</a>--}}
          <form action="{{ route('admin.product.destroy', $product->id)}}" method="post">
            @csrf
            @method('DELETE')
            <button class="btn btn-danger" type="submit">Удалить</button>
          </form>
        </td>
      </tr>

    @endforeach

  </table>
  {!! $products->links() !!}

@endsection