@extends('admin.default')

@section('content')

  <h3><?/*=$this->form_title*/?></h3>
  <?php //include "message.tpl"; ?>

  <div class="form">

    @if(isset($product))

      <form name="product" action="{{ route('admin.product.update') }}" method="POST" enctype="multipart/form-data">

        @method('PUT')

    @else

      <form name="product" action="{{ route('admin.product.save') }}" method="POST" enctype="multipart/form-data">

    @endif

      @csrf

      <table>
        <tr>
          <td>Раздел:</td>
          <td>
            <select name="category_id">
              @foreach($categories as $key => $category)
                <option value="{{ $category->id }}" @if($category->id == $product->category_id) selected="selected" @endif >{{ $category->title }}</option>
              @endforeach
            </select>
          </td>
        </tr>
        <tr>
          <td>Название:</td>
          <td>
            <input type="text" name="title" value="{{ isset($product) ? $product->title : '' }}" />
          </td>
        </tr>
        <tr>
          <td>Цена:</td>
          <td>
            <input type="text" name="price" value="{{ isset($product) ? $product->price : '' }}" /> грн
          </td>
        </tr>
        <tr>
          <td>Год:</td>
          <td>
            <input type="text" name="year" value="{{ isset($product) ? $product->year : '' }}" />
          </td>
        </tr>
        <tr>
          <td>Страна:</td>
          <td>
            <input type="text" name="country" value="{{ isset($product) ? $product->country : '' }}" />
          </td>
        </tr>
        <tr>
          <td>Режиссёр:</td>
          <td>
            <input type="text" name="director" value="{{ isset($product) ? $product->director : '' }}" />
          </td>
        </tr>
        <tr>
          <td>Продолжительность:</td>
          <td>
            <input type="text" name="play" value="{{ isset($product) ? $product->play : '' }}" />
          </td>
        </tr>
        <tr>
          <td>В ролях:</td>
          <td>
            <textarea name="cast" cols="30" rows="10">{{ isset($product) ? $product->cast : '' }}</textarea>
          </td>
        </tr>
        <tr>
          <td>Описание:</td>
          <td>
            <textarea name="description" cols="30" rows="10">{{ isset($product) ? $product->description : '' }}</textarea>
          </td>
        </tr>
        <tr>
          <td>Изображение:</td>
          <td>
            @if(isset($product))
              <img src="/images/products/{{ $product->image }}" alt="{{ $product->title }}">
            @endif
            <input type="file" name="image" />
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <input type="hidden" name="id" value="{{ isset($product) ? $product->id : '' }}" />
  {{--          <input type="hidden" name="func" value="<?=$this->func?>" />--}}
            <input type="submit" value="Отправить" />
          </td>
        </tr>
      </table>
    </form>
  </div>

@endsection