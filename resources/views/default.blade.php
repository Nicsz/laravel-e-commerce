<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title><?/*=$this->title*/?></title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="<?/*=$this->meta_desc*/?>" />
  <meta name="keywords" content="<?/*=$this->meta_key*/?>" />

  <!-- Bootstrap -->
{{--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">--}}

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">

  <link rel="stylesheet" href="{{ asset('css/main.css') }}" type="text/css" />
  <!--[if IE 8]>-->
  <link rel="stylesheet" href="{{ asset('css/ie8.css') }}" type="text/css" />
  <!--<![endif]-->

  <!-- Scripts -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script type="text/javascript" src="{{ asset('js/functions.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/test.js') }}"></script>
  <!-- End scripts -->

  <link href="{{ asset('favicon.ico') }}" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
<div id="container">

 {{-- <div class="container">
    <div class="row">
      <div class="col-lg-12 col-sm-12 col-12 main-section">
        <div class="dropdown">
          <button type="button" class="btn btn-info" data-toggle="dropdown">
            <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
          </button>
          <div class="dropdown-menu">
            <div class="row total-header-section">
              <div class="col-lg-6 col-sm-6 col-6">
                <i class="fa fa-shopping-cart" aria-hidden="true"></i> <span class="badge badge-pill badge-danger">{{ count((array) session('cart')) }}</span>
              </div>

              <?php $total = 0 ?>
              @foreach((array) session('cart') as $id => $details)
                <?php $total += $details['price'] * $details['quantity'] ?>
              @endforeach

              <div class="col-lg-6 col-sm-6 col-6 total-section text-right">
                <p>Total: <span class="text-info">$ {{ $total }}</span></p>
              </div>
            </div>

            @if(session('cart'))
              @foreach(session('cart') as $id => $details)
                <div class="row cart-detail">
                  <div class="col-lg-4 col-sm-4 col-4 cart-detail-img">
                    <img src="{{ asset('/img/default-1.png') }}" alt="item-image">
                  </div>
                  <div class="col-lg-8 col-sm-8 col-8 cart-detail-product">
                    <p>{{ $details['name'] }}</p>
                    <span class="price text-info"> ${{ $details['price'] }}</span> <span class="count"> Quantity:{{ $details['quantity'] }}</span>
                  </div>
                </div>
              @endforeach
            @endif
            <div class="row">
              <div class="col-lg-12 col-sm-12 col-12 text-center checkout">
                <a href="{{ url('cart') }}" class="btn btn-primary btn-block">View all</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>--}}

  @include('header')

  <div id="content">
    <div id="left">

      @include('sidebar')

    </div>
    <div id="right">

      @yield('content')

    </div>

    <div class="clear"></div>

    @include('footer')

    @yield('scripts')

  </div>
</div>

</body>
</html>