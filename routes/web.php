<?php

use Illuminate\Support\Facades\Route;

/** @var  \Illuminate\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('main');
});*/

$router->get('/', 'HomeController@index')->name('home');

$router->get('/product/{id}', 'ProductController@show')->name('product');
$router->get('/category/{id}', 'CategoryController@showCategory')->name('category');
$router->get('/category/{id}/sort', 'CategoryController@sort')->name('sort-category-products');
$router->get('/sort','HomeController@index')->name('sort-products');
$router->get('/search', 'ProductController@searchProduct')->name('search-products');

$router->get('/contacts', function () {
  return view('pages.contacts', ['categories' => \App\Category::all()]);
})->name('contacts');

$router->get('/delivery', function () {
  return view('pages.delivery', ['categories' => \App\Category::all()]);
})->name('delivery');

$router->get('/order', "OrderController@index")->name('order');

//Cart
$router->get('/cart', "CartController@cart")->name('cart');
$router->get('/add-to-cart/{id}', "CartController@addToCart")->name('cart.add');
$router->patch('/update-cart', 'CartController@update')->name('cart.update');
$router->delete('/remove-from-cart', 'CartController@remove')->name('cart.remove');

// Admin

$router->get('/dashboard', "Admin\AdminController@index")->name('admin.main');

Route::prefix('admin')->group(function () {
  Route::get('/auth', "Admin\AdminController@auth")->name('admin.auth');
  Route::post('/login', "Admin\AdminController@login")->name('admin.login');

  Route::get('/exit', "Admin\AdminController@adminExit")->name('admin.exit');

  //Product
  Route::get('/products', "Admin\ProductController@show")->name('admin.products');
  Route::get('/product/new', "Admin\ProductController@create")->name('admin.product.new');
  Route::post('/product/save', "Admin\ProductController@store")->name('admin.product.save');
  Route::get('/product/{id}/edit', "Admin\ProductController@edit")->name('admin.product.edit');
  Route::put('/product/update', "Admin\ProductController@update")->name('admin.product.update');
  Route::delete('/product/{id}/delete', "Admin\ProductController@destroy")->name('admin.product.destroy');

  //Category
  Route::get('/categories', "Admin\CategoryController@show")->name('admin.categories');
  Route::get('/category/new', "Admin\CategoryController@create")->name('admin.category.new');
  Route::post('/category/save', "Admin\CategoryController@store")->name('admin.category.save');
  Route::get('/category/{id}/edit', "Admin\CategoryController@edit")->name('admin.category.edit');
  Route::put('/category/update', "Admin\CategoryController@update")->name('admin.category.update');
  Route::delete('/category/{id}/delete', "Admin\CategoryController@destroy")->name('admin.category.destroy');
});