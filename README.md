# Laravel E-commerce

A Laravel E-commerce web application.

## System Requirements

The following are required to function properly.

- PHP 7.3
- MySQL 5.6

## Installation

<pre>
1. Clone the repo and cd into it
2. Composer install
3. Rename or copy .env.example file to .env
4. Php artisan key:generate
5. Set your database credentials in your .env file
6. Set your Admin credentials in your .env file. Specifically ADMIN_SECRET, ADMIN_LOGIN and ADMIN_PASSWORD
7. Php artisan serve
8. Visit localhost:8000 in your browser
9. Visit /admin if you want to access admin dashboard
</pre>

##Admin/Dashboard Area

####Category Management:

<pre>
-- Add Category;
-- Edit Category;
-- Delete Category;
-- Manage Category.
</pre>

####Product Management:

<pre>
-- Product Image;
-- Set Feature's Product;
-- Product Description;
-- Product Stock Management;
-- Discount Set;
-- Add Product;
-- Edit Product;
-- Delete Product;
-- Manage Product Category.
</pre>

####Orders Management:

<pre>
-- View All Order.
</pre>

##Frontend

<pre>
-- Fully Responsive Design;
-- Easy to Use Menu;
-- Delighted Product View Section;
-- Order system;
-- SEO Friendly URL;
-- Easy to order system.
</pre>